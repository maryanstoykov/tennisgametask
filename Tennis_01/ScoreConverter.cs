﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis_01
{
    public static class ScoreConverter
    {
        private static string ConvertPoints(int points)
        {
            string scoreString = "";

            switch (points)
            {
                case 0: scoreString = "Love"; break;
                case 1: scoreString = "Fifteen"; break;
                case 2: scoreString = "Thirty"; break;
                default: scoreString = "Forty"; break;
            }
            return scoreString;
        }

        public static string GameScore(int server, int receiver)
        {

            if ((server == receiver) && (server >= 3))
            {
                return "0";// 0 for duce
            }
            if ((server > receiver) && (server > 3) && (receiver >= 3) && (server-receiver < 2))
            {
                return "1"; // 1 for advantage in (advantage server)
            }
            if ((server < receiver) && (receiver > 3) && (server >= 3)&&(receiver - server <2))
            {
                return "2";//2 for advantage out (receiver advantage)
            }
            if ((server > 3) && (server - receiver >= 2))
            {
                return "3";//server wins the game
            }
            if ((receiver > 3) && (receiver - server >= 2))
            {
                return "4";//receiver wins the game
            }

            return ConvertPoints(server) + ":" + ConvertPoints(receiver);// retrun score converted to tennis score
        }
    }
}
