﻿using System;


namespace Tennis_01
{
    public class TennisGame
    {
        private TennisPlayer PlayerServer; // the player who has the service
        private TennisPlayer PlayerReceiver;
        private Random rnd=new Random(); 
     
   
    
        public TennisGame()
        {
            PlayerServer = new TennisPlayer("Player 1");
            PlayerReceiver = new TennisPlayer("Player 2");
            
        }
        public TennisGame(string name_server, string name_receiver)
        {
            PlayerServer = new TennisPlayer(name_server);
            PlayerReceiver = new TennisPlayer(name_receiver);
        }
      
        public void PlayGame()
        {
            Console.WriteLine("\n\n{0} service\n\t\tLOVE ALL\n================================",PlayerServer.Name);
            RandomPoint();
        }
        private void RandomPoint() 
        {
            

            int randomPoint = rnd.Next(1,101);

            //randomly assign point to one of the players
            if (randomPoint<=50)  
            { 
                Console.WriteLine("{0} scores",PlayerServer.Name);
                PlayerServer.PointsWon++;
            } else
            {
                Console.WriteLine("{0} scores",PlayerReceiver.Name);
                PlayerReceiver.PointsWon++;
            }
           
            CallScore(); // call score
        }

        private void CallScore()
        {
            string score = ScoreConverter.GameScore(PlayerServer.PointsWon, PlayerReceiver.PointsWon);

            string callscore = "";

            switch (score)
            {
                case "0":
                    callscore = "DUCE";
                    break;
                case "1":
                    callscore = "Advantage "+PlayerServer.Name;
                    break;
                case "2":
                    callscore = "Advantage " + PlayerReceiver.Name;
                    break;
                case "3":
                    callscore = "Win for "+PlayerServer.Name;
                    break;
                case "4":
                    callscore = "Win for " + PlayerReceiver.Name;
                    break;
                default:
                    callscore = score;
                    break;
            }

            
            Console.WriteLine("\t\t{0}",callscore);
            Console.WriteLine("-------------------------------------------");

            if ((!score.Equals("3"))&&(!score.Equals("4"))){// if there is no winner
                RandomPoint();// assign random point
            }
        }
    }
}
