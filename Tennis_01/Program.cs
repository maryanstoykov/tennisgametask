﻿using System;

namespace Tennis_01
{
    class Program
    {
        static void Main(string[] args)
        {
            string again = "";

            do
            {
                //TennisGame game = new TennisGame("Federer","Djokovic");
                TennisGame game = new TennisGame();

                game.PlayGame();

                Console.Write("Again (y/n)? ");
                again = Console.ReadLine();
                again = again.ToLower();

            } while (again.Equals("y"));

        }
    }
}
