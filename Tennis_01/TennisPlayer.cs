﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis_01
{
    class TennisPlayer
    {
        public int PointsWon { get; set; }
        public string Name { get; set; }

        public TennisPlayer(string name)
        {
            PointsWon = 0;
            Name = name;
        }
    }
}
